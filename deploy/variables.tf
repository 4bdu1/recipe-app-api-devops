variable "prefix" {
  type        = string
  default     = "raad"
  description = "prefix for aws resources"
}

variable "project" {
  type        = string
  default     = "recipe-app-api-devops"
  description = "project name"
}

variable "contact" {
  type        = string
  default     = "abdulaziztetteh@gmail.com"
  description = "contact of infra manager"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  type        = string
  default     = "recipe-app-api-devops-bastion"
  description = "key name for bastion instance"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "161623601844.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for proxy"
  default     = "161623601844.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
